//
//  Imagens.swift
//  Contato
//
//  Created by COTEMIG on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher
struct dogs: Decodable {
    let message:String
    let status:String
}
class Imagens: UIViewController {

    @IBOutlet weak var imgDogs: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getNewDogs()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func recarregarCachorro(_ sender: Any) {
    
        getNewDogs()
    }
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    func getNewDogs(){
        self.activityIndicator.startAnimating()
        AF.request("https://dog.ceo/api/breeds/image/random")
            .responseDecodable(of: dogs.self){
                response in

                if let dog = response.value{
                    self.imgDogs.kf.setImage(with: URL(string: dog.message)) { _ in
                        self.activityIndicator.stopAnimating()
                    }
                    
                }
            }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
