//
//  DetalhesViewControllerDelegate.swift
//  Contato
//
//  Created by COTEMIG on 08/09/22.
//

import UIKit
protocol <#name#> {
    <#requirements#>
}
class DetalhesViewControllerDelegate: UIViewController {
    public var index:Int?
    public var contato:Contato?
    public var delegate:DetalhesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func excluircontato(index:Int)
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        delegate?.excluirContato(index: index!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
